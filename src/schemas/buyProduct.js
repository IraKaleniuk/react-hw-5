import * as Yup from "yup";
import { baseStringValidation } from "./baseValidation";

export const buyProductSchema = Yup.object({
  firstName: baseStringValidation,
  lastName: baseStringValidation,
  age: Yup.number()
    .min(18, "Minimal age is 18")
    .max(99, "Maximum  age is 99")
    .required("This field required"),
  address: Yup.string()
    .matches(
      /^[A-Za-zА-ЯҐЄІЇа-яґєії,\.\d\s\/]+$/,
      "Only letters, numbers or , . /"
    )
    .required("This field is  required"),
  phone: Yup.string().required("This field is required"),
});
