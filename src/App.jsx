import "./App.scss";
import Header from "./components/header";
import { Route, Routes } from "react-router-dom";
import { Cart, Favorites, Home } from "./pages";
import { useEffect } from "react";
import { getProductsAsync } from "./redux/actions/products";
import { useDispatch, useSelector } from "react-redux";
import Modal from "./components/modal";
import { closeModal } from "./redux/actions/modal";

function App() {
  const dispatch = useDispatch();
  const { modalIsOpen, modalData } = useSelector((state) => state.modal);

  useEffect(() => {
    dispatch(getProductsAsync());
  }, [dispatch]);
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/favorites" element={<Favorites />} />
      </Routes>
      {modalIsOpen && (
        <Modal
          header={modalData.header}
          text={modalData.text}
          actions={modalData.actions}
          closeButton={modalData.closeButton}
          closeButtonHandler={() =>
            function () {
              dispatch(closeModal());
            }
          }
        />
      )}
    </>
  );
}

export default App;
