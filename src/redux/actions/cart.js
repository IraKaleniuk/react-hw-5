import { cartTypes } from "../types";

export function addToCart(id) {
  return function (dispatch) {
    dispatch({ type: cartTypes.ADD_TO_CART, payload: { id } });
  };
}

export function deleteFromCart(id) {
  return function (dispatch) {
    dispatch({
      type: cartTypes.DELETE_FROM_CART,
      payload: { id },
    });
  };
}
export function clearCart(target) {
  return function (dispatch) {
    dispatch({
      type: cartTypes.CLEAR_CART,
      payload: { target },
    });
  };
}
